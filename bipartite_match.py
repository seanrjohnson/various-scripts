'''
    A python implementation of the Bipartite Matching Algorithm
    from Combinatorial Optimization: Algorithms and Complexity
    by Papadimitriou and Steiglitz
    Figure 10-3
    
    Sean Johnson July 14, 2013
'''
import pdb

def maximum_match(E):
    '''
        Find a maximum matching between left nodes and right nodes in a bipartite graph,
        In other words:
        given a list of edges, where each edge consists of one left node and one right node,
        find the largest subset of those edges such that each node is present no more than once.
        (a maximum matching)
        (Note that there may be more than one possible maximum matching, but only one is returned)
        
        Input: edges
            a list of edges represented as 2-tuples where the first number is the index of 
            the left node, and the second number is the index of the right node.
            
        Returns: a list of edges in the maximum matching
        
        Precondition: all entries in E should >= 0.  
        Also, it will be most efficient if they are as small as possible.
        (i.e. translate their indexes to be as close to zero as possible)
    '''
    #if E is empty, return an empty list
    if len(E) == 0:
        return []
    
    #first find the maxiumum index of the right and left lists
    l_len = 0
    r_len = 0
    for t in E:
        #TODO: check if node names names are >= 0
        if t[0] > l_len:
            l_len = t[0]
        if t[1] > r_len:
            r_len = t[1]
    
    l_len += 1
    r_len += 1
    
    #now make the initial 'mate' lists
    l_mate = [-1] * l_len
    r_mate = [-1] * r_len
    
    return _stage(E, l_mate, r_mate)
    
def _stage(E, l_mate, r_mate):
    #print l_mate
    #print r_mate
    label = [-1] * len(l_mate) #
    exposed = [-1] * len(l_mate)
    A = set() #set of edges in the auxiliary graph
    #pdb.set_trace()
    for (l, r) in E:
        if r_mate[r] == -1:
            exposed[l] = r
        else:
            if r_mate[r] != l:
                A.add((l, r_mate[r]))
    Q = set() #a stack of left nodes with no match
    for l in xrange(len(l_mate)):
        if l_mate[l] == -1: #l has no match
            Q.add(l)
            #label[l] = -1
    #print exposed
    while len(Q) > 0:
        l = Q.pop()
        if exposed[l] != -1:
            _augment(l, label, l_mate, r_mate, exposed)
            return _stage(E, l_mate, r_mate)
        else:
            for (l_1, l_prime) in A:
                if (l_1 == l) and (label[l_prime] == -1):
                    label[l_prime] = l_1
                    Q.add(l_prime)
    #pdb.set_trace()                
    return [(i, l_mate[i]) for i in xrange(len(l_mate)) if l_mate[i] != -1]
    
        
def _augment(l, label, l_mate, r_mate, exposed):
    # label, mate, exposed,
    if label[l] == -1:
        l_mate[l] = exposed[l]
        r_mate[exposed[l]] = l
    else:
        exposed[label[l]] = l_mate[l]
        l_mate[l] = exposed[l]
        r_mate[exposed[l]] = l
        _augment(label[l], label, l_mate, r_mate, exposed)

def test():
    e1 = [(0,0), (0,1), (1,0), (1,1), (2,1), (2,2)]
    e2 = [(1,1), (1,2), (2,1), (2,2), (3,2), (3,3)]
    e3 = [(1,1), (1,2), (1,4), (2,2), (2,6), (3,2), (3,3), (4,3), (4,5),(4,6), (5,3), (5,4), (5,5), (5,6), (6,2), (6,5)]
    
    print maximum_match(e1)
    print maximum_match(e2)
    print maximum_match(e3)
        
if __name__ == "__main__":
    test()
