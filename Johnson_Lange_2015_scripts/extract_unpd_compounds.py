import pandas as pd
import get_standard_inchi

infile_name = "E:/databases/for_review/unpd/UNPD_unique_information_229358_prog.csv"
database_name = "UNPD"

outfile_name = "UNPD_compounds_part3.tsv"

table = pd.read_csv(infile_name, sep="\t", low_memory=False)
#recs = parse_gmd_file(data_dir + "/" + MSP_file_name)
with open(outfile_name, "w") as outfile:
    #pp([x for x in glob(data_dir + "/" + "*.mgf")])
    for (rownum, row) in table.iterrows():
        
        inchi = get_standard_inchi.standardize_inchi(row['InChI'])
        accession = row['unpdid']
        outfile.write(database_name + "\t" + inchi + "\t" + accession + "\t" + "\n")