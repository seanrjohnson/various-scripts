from xml.dom.minidom import parse
from glob import iglob as glob
import codecs

record_dir = "E:/databases/for_review/hmdb"
outfile_name = "HMDB_compounds_with_class_all.tsv"
database_name = "HMDB"

with codecs.open(outfile_name, "w", "utf-8-sig") as outfile:
    for infile_name in glob(record_dir+"/*.xml"):
        rec = parse(infile_name)
        accession = rec.getElementsByTagName("accession")[0].firstChild.data
        print(accession)
        name = ""
        spectra = set()
        superclass = ""
        subclass = ""
        inchi = ""
        if rec.getElementsByTagName("inchi")[0].firstChild is not None:
            inchi = rec.getElementsByTagName("inchi")[0].firstChild.data
        if rec.getElementsByTagName("name")[0].firstChild is not None:
            name = rec.getElementsByTagName("name")[0].firstChild.data
        if rec.getElementsByTagName("super_class")[0].firstChild is not None:
            superclass = rec.getElementsByTagName("super_class")[0].firstChild.data
        if rec.getElementsByTagName("class")[0].firstChild is not None:
            subclass = rec.getElementsByTagName("class")[0].firstChild.data
        outfile.write(database_name + "\t" + inchi + "\t" + accession + "\t" + name + "\t" + superclass + "\t" + subclass + "\n")

            
