import pandas as pd
from glob import iglob as glob
import re
from pprint import pprint as pp
from numpy import isnan
table_file_suffix = "_compounds.tsv"
hmdb_record_file = "HMDB_compounds_with_class_all.tsv"
GMD_file = "GMD_compounds.tsv"

tables = dict() #key is database name, value is pandas dataframe
unique_skeletons = dict() #key is database name, value is a set of the unique skeletons in the dataset
table_comparison = dict() #2D dict, keys are table names, values are spectra in common / total spectra in table_1

gmd = pd.read_table(GMD_file, header=None)
gmd.columns=['database', 'inchi', 'accession', 'type', 'path']

hmdb = pd.read_table(hmdb_record_file, header=None)
hmdb.columns=['database', 'inchi', 'accession', 'name', 'class1', 'class2']
for x in (gmd, hmdb):
    x['inchi'] = x['inchi'].str.extract('InChI=1S/([^/]+/[^/]+(?:/h[^/]+)?).*').str.replace('?','')
    x.drop_duplicates(subset='inchi', inplace=True)
joined = gmd.merge(hmdb, how='left', on='inchi', suffixes=('gmd', 'hmdb'))


annotations = dict() #a dict of dicts of ints, first key is class1, second key is class2, value is how many of them there are
for (i, r) in joined.iterrows():
    class1 = r['class1']
    class2 = r['class2']
    
    if class1 not in annotations:
        annotations[class1] = dict()
    if class2 not in annotations[class1]:
        annotations[class1][class2] = 0
    annotations[class1][class2] += 1

for c1 in annotations:
    total = 0
    for c2 in annotations[c1]:
        total += annotations[c1][c2]
        print(str(c1) + "\t" + str(c2) + "\t" + str(annotations[c1][c2]))
    print(str(c1) + "\t" + "\t" + str(total))