import extract_massbank as massbank
from glob import iglob as glob
import get_standard_inchi

massbank_record_dir = "E:/databases/for_review/respect"
outfile_name = "ReSpect_compounds.tsv"
database_name = "ReSpect"

null_fields = set(["", "N/A", "no data"])

with open(outfile_name, "w") as outfile:
    for infile_name in glob(massbank_record_dir+"/*.txt"):
        with open(infile_name, "r") as infile:
            mb = massbank.MassBank(infile)
            inchi = ""
            if 'CH$INCHI' in mb.data and mb.data["CH$INCHI"][0] not in null_fields:
                inchi = get_standard_inchi.standardize_inchi(mb.data["CH$INCHI"][0])
            elif 'CH$IUPAC' in mb.data and mb.data["CH$IUPAC"][0] not in null_fields:
                inchi = get_standard_inchi.standardize_inchi(mb.data["CH$IUPAC"][0])
            elif "CH$SMILES" in mb.data and mb.data["CH$SMILES"][0] not in null_fields:
                inchi = get_standard_inchi.smiles_to_standard_inchi(mb.data["CH$SMILES"][0])
            
            mstype = mb.data['RECORD_TITLE'][1]
            # cas = ""
            # if 'CH$LINK CAS' in mb.data:
                # cas = mb.data['CH$LINK CAS'][0]
            #outfile.write(database_name + "\t" + inchi + "\t" + mb.accession() + "\t" + mstype + "\t" + cas +"\n")
            if inchi != "":
                outfile.write(database_name + "\t" + inchi + "\t" + mb.accession() + "\t" + mstype + "\n")
            


 
