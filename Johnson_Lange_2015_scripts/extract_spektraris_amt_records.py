import extract_massbank as massbank
from glob import iglob as glob
massbank_record_dir = "E:/databases/for_review/spektraris_amt"
outfile_name = "spektraris_amt_compounds.tsv"
database_name = "Spektraris"

with open(outfile_name, "w") as outfile:
    for infile_name in glob(massbank_record_dir+"/*.txt"):
        with open(infile_name, "r") as infile:
            mb = massbank.MassBank(infile)
            outfile.write(database_name + "\t" + mb.inchi() + "\t" + mb.accession() + "\t" + mb.spectrum_type() + "\n")


 
