import pandas as pd
from glob import iglob as glob
import re
from pprint import pprint as pp
from extract_chebi_compound_classes import Obo
table_file_suffix = "_compounds.tsv"
output_dir = "E:\\databases\\for_review\\analysis"
outfile_name = "data_charts_chebi_numbers.tsv"
chebi_obo_filename = 'chebi.obo'

chebi = Obo(chebi_obo_filename)

tables = dict() #key is database name, value is pandas dataframe
unique_skeletons = dict() #key is database name, value is a set of the unique skeletons in the dataset
table_comparison = dict() #2D dict, keys are table names, values are spectra in common / total spectra in table_1

all_skeletons = set()
combined_table = pd.DataFrame()
spectra_types = list()

#read all of the compounds.tsv files, and load them into data frames
for filename in glob("*" + table_file_suffix):
    #if "UNPD" not in filename and "NMRShiftDB" not in filename:
    if "UNPD" not in filename and "NMRShiftDB" not in filename:
    #if "ChEBI" in filename:
        database_name = filename[:-1*len(table_file_suffix)]
        tables[database_name] = pd.read_table(filename, header=None)
        tables[database_name][3] = tables[database_name][3].replace(re.compile('MS[2-9]'), 'MSn')
        
        skeletons = tables[database_name][1].str.extract(r'(InChI=1S/[^/]+/[^/]+(?:/h[^/]+)?).*').str.replace('?','')
        tables[database_name]['skeletons'] = skeletons
        all_skeletons.update(skeletons)
        unique_skeletons[database_name] = set(skeletons.drop_duplicates())
        tables[database_name] = tables[database_name].drop_duplicates(subset=[3,'skeletons'])
        combined_table = pd.concat([combined_table, tables[database_name]], ignore_index=True)
spectra_types = sorted(set(combined_table[3]))


#get the compound classes from the ChEBI obo file
chebi_classes = dict() #key is chebi ID, value is a set of classifiers
for rec in chebi:
    #print(chebi[rec]._data)
    inchi = chebi[rec].inchi()
    
    if inchi:
        id = chebi[rec].id
        chebi_classes[id] = set()
        ancestors = chebi.ancestors(id)
        all_classes = ""
        #all_classes = set()
        for anc in ancestors:
            all_classes += anc + " " + chebi[anc].name() + "\t"
            #all_classes.add(anc + " " + chebi[anc].name())
        chebi_classes[id] = all_classes
chebi_classes = pd.DataFrame.from_dict(chebi_classes,orient='index')
chebi_classes.columns = ['classes']
tables['ChEBI'] = pd.merge(tables['ChEBI'], chebi_classes, left_on=2, right_index=True)


#tables["Combined"] = combined_table.drop_duplicates(subset=[3,'skeletons'])
#unique_skeletons["Combined"] = set(tables["Combined"]["skeletons"].drop_duplicates())

for (name, skels) in unique_skeletons.iteritems():
    table_comparison[name] = dict()
    for (name2, skels2) in unique_skeletons.iteritems(): #this will do twice as much work as necessary, but they are quick operations, so that's ok...
        table_comparison[name][name2] = skels.intersection(skels2)

chebi_annotations = dict() #a dict of dicts of ints, first key is database, second key is ChEBI
for name in table_comparison:
    chebi_annotations[name] = dict()

    for skel in table_comparison[name]['ChEBI']:
        local_annotations = set()
        classes_array = tables['ChEBI'][tables['ChEBI']['skeletons'] == skel]['classes']
        for entry in classes_array:
            local_annotations.update(entry.split("\t"))
        for entry in local_annotations:
            if entry not in chebi_annotations[name]:
                chebi_annotations[name][entry] = 0
            chebi_annotations[name][entry] += 1
    print(name)
    for (annotation, count) in chebi_annotations[name].items():
        print(str(count) + "\t" + annotation)

# with open(output_dir + "\\" + outfile_name, "w") as outfile:
    # table_names = sorted(unique_skeletons.keys())
    # outfile.write("\t"+"Total"+ "\t" +"\t".join(table_names)+"\n")
    # for name in table_names:
        # outfile.write(name)
        # outfile.write("\t" + str(len(unique_skeletons[name])))
        # for name2 in table_names:
            # #outfile.write("\t"+ "%0.5f" % table_comparison[name][name2])
            # outfile.write("\t"+ table_comparison[name][name2])
            
        # outfile.write("\n")
    # #outfile.write("Total Unique"+"\t"+str(len(all_skeletons)))
    # outfile.write("\n")
    
    # outfile.write("\t"+"\t".join(spectra_types)+"\n")
    # for name in table_names:
        # outfile.write(name)
        # for type in spectra_types:
            # skeletons_with_spectra = len(tables[name][tables[name][3] == type])
            # outfile.write("\t" + str(skeletons_with_spectra))
        # outfile.write("\n")
    

