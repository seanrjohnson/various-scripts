import get_standard_inchi
import re


record_dir = "E:/databases/for_review/nmrshiftdb"
outfile_name = "nmrshiftdb_compounds.tsv"
database_name = "NMRShiftDB"
molecule_file_name = "molecules.txt"
#spectra_file_name = "spectra.txt"

spectrum_type_dict = {'1':'13C NMR', '2':'1H NMR', '3':'1D NMR other', '4':'1D NMR other', '5':'1D NMR other', '6':'1D NMR other', '7':'1D NMR other', '8':'1D NMR other', '9':'1D NMR other', '10':'1D NMR other', '11':'1D NMR other', '100':'1D NMR other', '101':'1D NMR other'}

molecules = dict() #keys are molecule_ID, values are InChI
with open("nmrshiftdb_molecule_to_inchi.tsv", "w") as outfile:
    with open(record_dir + "/" + molecule_file_name, "r") as infile:
        for line in infile:
            match = re.search('^INSERT INTO `MOLECULE` VALUES \((.+)\);$', line)
            if match:
                fields = match.group(1).split(',')
                smiles = fields[4].strip("'")
                ID = fields[0]
                outfile.write(ID + "\t" + get_standard_inchi.smiles_to_standard_inchi(smiles) + "\n")
            else:
                print("error reading line: %s" % line)

