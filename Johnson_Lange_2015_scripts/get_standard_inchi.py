'''
    functions for calling ChemAxon molconvert for converting inchi into standard inchi, and smiles into inchi
'''
import subprocess

def smiles_to_standard_inchi(instring):
    '''
        input: a SMILES string
        output: a Standard InChI string
    '''
    exec_string = 'molconvert inchi:"AuxNone SAbs"'
    process = subprocess.Popen(exec_string, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
    out, err = process.communicate(instring)
    if err:
        print(err.strip())
    return out.strip()

def standardize_inchi(instring):
    '''
        input: an InChI string
        output: a Standard InChI string
        
        precondition: input must actually be an InChI string. This function does not check that the input is valid, and may give uninterpretable results or errors if the input is not an InChI string
    '''
    out = instring.strip()
    #print(instring)
    if out != "":
        if not out.startswith("InChI="):
            print("InChI= added")
            out = "InChI=" + out
        if not out.startswith("InChI=1S"):
            print("InChI converted to Standard InChI")
            out = inchi_to_standard_inchi(out)

    return out
    
def inchi_to_standard_inchi(instring):
    exec_string = 'molconvert inchi:"AuxNone SAbs"'
    process = subprocess.Popen(exec_string, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
    out, err = process.communicate(instring)
    if err:
        print(err.strip())
    return out.strip()

if __name__ == "__main__":
    print(smiles_to_standard_inchi("C1(C(C(OC(C1O)O)C(=O)O)O)O"))
    print(inchi_to_standard_inchi("InChI=1S/C6H9N3O2/c7-5(6(10)11)1-4-2-8-3-9-4/h2-3,5H,1,7H2,(H,8,9)(H,10,11)/t5-/m0/s1/f/h9-10H"))
