import re
from pprint import pprint as pp
from glob import iglob as glob
import get_standard_inchi

data_dir = "E:/databases/for_review/gnps"
database_name = "GNPS"

outfile_name = "GNPS_compounds.tsv"

null_fields = set(["", "N/A", "no data"])

def parse_gmd_file(filename):
    recs = list()
    with open(filename, "r") as infile:
        text = infile.read()
        recs_text = re.split('END IONS', text)
        for rec in recs_text:
            #pp(rec)
            recs.append(parse_gmd_record(rec))
    #pp(recs)
    return recs


def parse_gmd_record(rec_text):
    out = dict()
    out['inchi'] = ""
    out['accession'] = ""
    out['mslevel'] = ""
    inchi_match = re.search('\nINCHI=(.+)\n', rec_text)
    smiles_match = re.search('\nSMILES=(.+)\n', rec_text)
    accession_match = re.search('\nSPECTRUMID=(.+)\n', rec_text)
    ms_level_match = re.search('\nMSLEVEL=(.+)\n', rec_text)
    
    
    
    if smiles_match and inchi_match:
        if smiles_match.group(1) not in null_fields and inchi_match.group(1) in null_fields:
            #print(accession_match.group(1))
            #print(smiles_match.group(1))
            out['inchi'] = get_standard_inchi.smiles_to_standard_inchi(smiles_match.group(1))
        elif inchi_match.group(1) not in null_fields:
            inchi = inchi_match.group(1).strip("\"")
            out['inchi'] = get_standard_inchi.standardize_inchi(inchi)
            
        if accession_match.group(1) not in null_fields and out['inchi'] != "" and ms_level_match.group(1) not in null_fields:
            out['accession'] = accession_match.group(1)
            out['mslevel'] = ms_level_match.group(1)
    return out
    
    
#recs = parse_gmd_file(data_dir + "/" + MSP_file_name)
with open(outfile_name, "w") as outfile:

    #pp([x for x in glob(data_dir + "/" + "*.mgf")])
    for infile_name in glob(data_dir + "/" + "*.mgf"):
        print infile_name
        recs = parse_gmd_file(infile_name)
        for rec in recs:
            inchi = rec['inchi']
            accession = rec['accession']
            mslevel = rec['mslevel']
            if inchi != "" and accession != "":
                outfile.write(database_name + "\t" + inchi + "\t" + accession + "\t" + "MS" + mslevel + "\t" + infile_name + "\n")