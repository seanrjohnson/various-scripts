import extract_massbank as massbank
from glob import iglob as glob
massbank_record_dir = "E:/databases/for_review/massbank/all_records"
outfile_name = "MassBank_compounds.tsv"
database_name = "MassBank"

spectrum_type_map = {"MS":"MS1", "MS1":"MS1", "MS2":"MSn", "MS3":"MSn", "MS4":"MSn", }
with open(outfile_name, "w") as outfile:
    for infile_name in glob(massbank_record_dir+"/*.txt"):
        with open(infile_name, "r") as infile:
            mb = massbank.MassBank(infile)
            outfile.write(database_name + "\t" + mb.inchi() + "\t" + mb.accession() + "\t" + spectrum_type_map[mb.spectrum_type()] + "\n")


 
