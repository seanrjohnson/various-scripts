from xml.dom.minidom import parse
from glob import iglob as glob
record_dir = "E:/databases/for_review/hmdb"
outfile_name = "HMDB_compounds.tsv"
database_name = "HMDB"
oneD_spectra = set()
twoD_spectra = set()
oned_spectra_file_name = "hmdb_1d_nmr_accessions.tsv"
twod_spectra_file_name = "hmdb_2d_nmr_accessions.tsv"

spec_map_file = 'E:/databases/for_review/hmdb/spectra_types/extracted_spectrum_types.tsv'

#spec_list_2D_NMR = set()
spec_list_13C_NMR = set()
spec_list_1H_NMR = set()


with open(spec_map_file) as spec_in:
    for line in spec_in:
        line = line.strip()
        if line != "":
            line = line.split("\t")
            if "1H" in line[1]:
                spec_list_1H_NMR.add(line[0])
            if "13C" in line[1]:
                spec_list_13C_NMR.add(line[0])


with open(outfile_name, "w") as outfile:
    for infile_name in glob(record_dir+"/*.xml"):
        rec = parse(infile_name)
        accession = rec.getElementsByTagName("accession")[0].firstChild.data
        print(accession)
        inchi = ""
        if rec.getElementsByTagName("inchi")[0].firstChild is not None:
            inchi = rec.getElementsByTagName("inchi")[0].firstChild.data
        spectra = set()
        for spectrum in rec.getElementsByTagName("spectra")[0].getElementsByTagName("spectrum"):
            type = spectrum.getElementsByTagName("type")[0].firstChild.data
            id = spectrum.getElementsByTagName("spectrum_id")[0].firstChild.data
            if type == "Specdb::MsMs":
                spectra.add("MS2")
            elif type == "Specdb::CMs":
                spectra.add("MS1")
            # elif type == "Specdb::NmrOneD":
                # spectra.add("1D NMR")
                # oneD_spectra.add(id)
            elif type == "Specdb::NmrTwoD":
                spectra.add("2D NMR")
                twoD_spectra.add(id)
#            else:
#                print("warning unrecognized spectrum type: %s" % type)
        if accession in spec_list_1H_NMR:
            spectra.add("1H NMR")
        if accession in spec_list_13C_NMR:
            spectra.add("13C NMR")
        
        for spectrum in spectra:
            outfile.write(database_name + "\t" + inchi + "\t" + accession + "\t" + spectrum + "\n")

            
# with open(oned_spectra_file_name, "w") as outfile:
    # for spec in oneD_spectra:
        # outfile.write(spec + "\n")

# with open(twod_spectra_file_name, "w") as outfile:
    # for spec in twoD_spectra:
        # outfile.write(spec + "\n")