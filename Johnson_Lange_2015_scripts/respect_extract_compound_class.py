import extract_massbank as massbank
from glob import iglob as glob
import get_standard_inchi
from pprint import pprint as pp

massbank_record_dir = "E:/databases/for_review/respect"
database_name = "ReSpect"

null_fields = set(["", "N/A", "no data"])

compound_annotations = dict() #key is combination of all three classes, value is count
seen = set()
for infile_name in glob(massbank_record_dir+"/*.txt"):
    with open(infile_name, "r") as infile:
        mb = massbank.MassBank(infile)
        name = mb.data['RECORD_TITLE'][0]
        if 'CH$COMPOUND_CLASS' in mb.data and name not in seen: # and mb.data["CH$INCHI"][0] not in null_fields
            seen.add(name)
            sub_classes = list()
            for entry in mb.data["CH$COMPOUND_CLASS"]:
                (level, sep, cpd_class) = entry.partition(" ")
                if level != 'CLASS3':
                  sub_classes.append(cpd_class)
            annotation_string = ";".join(sub_classes)
            if annotation_string not in compound_annotations:
                compound_annotations[annotation_string] = 0
            compound_annotations[annotation_string] += 1
for (annot, count) in compound_annotations.iteritems():
    print(annot + "\t" + str(count))
            
# compound_classes = dict() #dict of dicts, first key is class level (CLASS1, CLASS2, CLASS3), second key is the compound class (Phenylpropanoid, etc), the value is how many records there are that match those criteria.

# for infile_name in glob(massbank_record_dir+"/*.txt"):
    # with open(infile_name, "r") as infile:
        # mb = massbank.MassBank(infile)
        # if 'CH$COMPOUND_CLASS' in mb.data: # and mb.data["CH$INCHI"][0] not in null_fields
            # for entry in mb.data["CH$COMPOUND_CLASS"]:
                # (level, sep, cpd_class) = entry.partition(" ")
                # if level not in compound_classes:
                    # compound_classes[level] = dict()
                # if cpd_class not in compound_classes[level]:
                    # compound_classes[level][cpd_class] = 0
                # compound_classes[level][cpd_class] += 1

# for (level, class_dict) in compound_classes.iteritems():
    # for (cpd_class, count) in class_dict.iteritems():
        # print(cpd_class + "\t" + str(count) + "\t" + level)