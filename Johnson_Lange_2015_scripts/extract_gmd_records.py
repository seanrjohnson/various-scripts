import re
from pprint import pprint as pp
from glob import iglob as glob
data_dir = "E:/databases/for_review/gmd"
database_name = "GMD"
#MSP_file_name = "GMD_20111121_MDN35_ALK_MSP.txt"

outfile_name = "GMD_compounds.tsv"

def parse_gmd_file(filename):
    recs = list()
    with open(filename, "r") as infile:
        text = infile.read()
        recs_text = re.split('\n\n', text)
        for rec in recs_text:
            #pp(rec)
            recs.append(parse_gmd_record(rec))
    #pp(recs)
    return recs


def parse_gmd_record(rec_text):
    out = dict()
    out['inchi'] = ""
    out['accession'] = ""
    inchi_match = re.search('\nSynon: METB InChI: (.+)\n', rec_text)
    accession_match = re.search('\nSynon: METB: (M[0-9]+)', rec_text)

    if accession_match and inchi_match:
        out['inchi'] = inchi_match.group(1)
        out['accession'] = accession_match.group(1)
    return out

    
#recs = parse_gmd_file(data_dir + "/" + MSP_file_name)
with open(outfile_name, "w") as outfile:
    spectrum = "MS1"
    for infile_name in glob(data_dir + "/" + "*MSP.txt"):
        recs = parse_gmd_file(infile_name)
        for rec in recs:
            inchi = rec['inchi']
            accession = rec['accession']
            if inchi != "" and accession != "":
                outfile.write(database_name + "\t" + inchi + "\t" + accession + "\t" + spectrum + "\t" + infile_name + "\n")