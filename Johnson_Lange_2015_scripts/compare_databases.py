import pandas as pd
from glob import iglob as glob
import re
table_file_suffix = "_compounds.tsv"
output_dir = "E:\\databases\\for_review\\analysis"
outfile_name = "data_charts_noshiftdb.tsv"

tables = dict() #key is database name, value is pandas dataframe
unique_skeletons = dict() #key is database name, value is a set of the unique skeletons in the dataset
table_comparison = dict() #2D dict, keys are table names, values are spectra in common / total spectra in table_1

all_skeletons = set()
combined_table = pd.DataFrame()
spectra_types = list()

for filename in glob("*" + table_file_suffix):
    #if "UNPD" not in filename and "NMRShiftDB" not in filename:
    if "UNPD" not in filename and "NMRShiftDB" not in filename and "ChEBI" not in filename:
        database_name = filename[:-1*len(table_file_suffix)]
        tables[database_name] = pd.read_table(filename, header=None)
        tables[database_name][3] = tables[database_name][3].replace(re.compile('MS[2-9]'), 'MSn')
        skeletons = tables[database_name][1].str.extract('InChI=1S/([^/]+/[^/]+(?:/h[^/]+)?).*').str.replace('?','')
        tables[database_name]['skeletons'] = skeletons
    #       print(list(skeletons))
        all_skeletons.update(skeletons)
        unique_skeletons[database_name] = set(skeletons.drop_duplicates())
        tables[database_name] = tables[database_name].drop_duplicates(subset=[3,'skeletons'])
        combined_table = pd.concat([combined_table, tables[database_name]], ignore_index=True)
spectra_types = sorted(set(combined_table[3]))

tables["Combined"] = combined_table.drop_duplicates(subset=[3,'skeletons'])
unique_skeletons["Combined"] = set(tables["Combined"]["skeletons"].drop_duplicates())

for (name, skels) in unique_skeletons.iteritems():
    table_comparison[name] = dict()
    for (name2, skels2) in unique_skeletons.iteritems():
        table_comparison[name][name2] = float(len(skels.intersection(skels2))) / float(len(skels))
        #table_comparison[name][name2] = str(len(skels.intersection(skels2))) + "/" + str(len(skels))
    

with open(output_dir + "\\" + outfile_name, "w") as outfile:
    table_names = sorted(unique_skeletons.keys())
    outfile.write("\t"+"Total"+ "\t" +"\t".join(table_names)+"\n")
    for name in table_names:
        outfile.write(name)
        outfile.write("\t" + str(len(unique_skeletons[name])))
        for name2 in table_names:
            outfile.write("\t"+ "%0.5f" % table_comparison[name][name2])
            #outfile.write("\t"+ table_comparison[name][name2])
            
        outfile.write("\n")
    #outfile.write("Total Unique"+"\t"+str(len(all_skeletons)))
    outfile.write("\n")
    
    outfile.write("\t"+"\t".join(spectra_types)+"\n")
    for name in table_names:
        outfile.write(name)
        for type in spectra_types:
            skeletons_with_spectra = len(tables[name][tables[name][3] == type])
            outfile.write("\t" + str(skeletons_with_spectra))
        outfile.write("\n")
    

