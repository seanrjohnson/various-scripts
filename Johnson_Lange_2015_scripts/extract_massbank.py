#!/usr/bin/python
"""
    A class for reading and storing data from MassBank format files.
    This is intended more as a library than as a standalone
"""
from __future__ import print_function
import sys
import argparse
#from pprint import pprint as pp
import json

class MassBank():
    single_tags = set(['ACCESSION','RECORD_TITLE','DATE','AUTHORS','LICENSE','COPYRIGHT','PUBLICATION','COMMENT','CH$NAME','CH$COMPOUND_CLASS','CH$FORMULA','CH$EXACT_MASS','CH$SMILES','CH$IUPAC','SP$SCIENTIFIC_NAME','SP$LINEAGE','SP$SAMPLE','AC$INSTRUMENT','AC$INSTRUMENT_TYPE','PK$ANNOTATION','PK$NUM_PEAK', 'PK$PEAK', "CH$INCHI", 'COMMENTS', 'STRUCTURE_FILE_NAME'])
    tags_with_subtags = set(['CH$LINK','SP$LINK','AC$MASS_SPECTROMETRY','AC$CHROMATOGRAPHY','MS$FOCUSED_ION','MS$DATA_PROCESSING', 'AC$ANALYTICAL_CONDITION'])
    multi_line_tags = set(['PK$ANNOTATION', 'PK$PEAK'])
    def __init__(self, in_stream):
        '''
            creates an object with the 'data' field, which is a dict of the format:
            keys are Tags (when there is a subtag, then the key is Tag Subtag), values are a list of values.
            Any data in the original MassBank file can be accessed through the ''data' field
        '''
        self.data = dict() #keys are Tags (when there is a subtag, then the key is Tag Subtag), values are a list of values.
        
        last_tag = None
        #state = "start"
        for line in (in_stream):
            line = line.rstrip()
            fields = line.partition(": ") #returns a tuple of (before separator, separator, after separator), if no separator is found then ("", "", string)
            if line[0:2] == "  ":
                if last_tag is not None and last_tag in self.multi_line_tags:
                    self.add_value(last_tag, line[2:])
                else:
                    print("WARNING: indented value found in non multi-line context", file=sys.stderr)
            elif fields[1] != "": #it has a tag
                last_tag = None
                if fields[0] in self.single_tags: #the tag has one part
                    last_tag = fields[0]
                    values = fields[2].split("; ")
                    for val in values:
                        self.add_value(last_tag, val)
                elif fields[0] in self.tags_with_subtags: #the tag includes a subtag
                    subtag_fields = fields[2].partition(" ")
                    if subtag_fields[0] == "":
                        print("WARNING: expected subtag for tag %s but no subtag found" % fields[0], file=sys.stderr)
                    else:
                        last_tag = fields[0] + " " + subtag_fields[0]
                        values = subtag_fields[2].split("; ")
                        for val in values:
                            self.add_value(last_tag, val)
                else:
                    print("WARNING: tag %s not recognized" % fields[0], file=sys.stderr)
                
            elif line[0:2] == "//": #end of record
                break
    def inchi(self):
        return self.data['CH$IUPAC'][0]
    def accession(self):
        return self.data['ACCESSION'][0]
    def spectrum_type(self):
        return self.data['AC$MASS_SPECTROMETRY MS_TYPE'][0]
    def add_value(self, tag, value):
        if tag not in self.data:
            self.data[tag] = []
        self.data[tag].append(value)

def main(infile, outfile):
    mb = MassBank(infile)
    #pp(mb.data, outfile)
    print(json.dumps(mb.data), file=outfile)

        
if __name__ == '__main__':
  ap = argparse.ArgumentParser()
  ap.add_argument('-i', default=None)
  ap.add_argument('-o', default=None)
  args = ap.parse_args()
  
  infile = sys.stdin
  outfile = sys.stdout
  
  if args.i is not None:
    infile = open(args.i, 'rb')
  if args.o is not None:
    outfile = open(args.o, 'w')
  
  main(infile, outfile)
  infile.close()
  outfile.close()