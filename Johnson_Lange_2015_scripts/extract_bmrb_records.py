import pandas as pd
import get_standard_inchi

data_dir = "E:/databases/for_review/bmrb"
database_name = "BMRB"

chem_comp = pd.read_csv(data_dir + "/Chem_comp.csv")
exp = pd.read_csv(data_dir + "/Experiment.csv")

merged = pd.merge(exp, chem_comp, on="Entry_ID", how="left")

outfile_name = "BMRB_compounds.tsv"

spectrum_map = {'1D 1H':'1H NMR', '1D 1H, 0.5 mM, pH 8.202':'1H NMR', '1D 1H, 100 mM, pH 8.202':'1H NMR', '1D 1H, 100 mM, pH 7.074':'1H NMR', '1D 1H, 100 mM, pH 6.836':'1H NMR', '1D 1H, 0.5 mM, pH 2':'1H NMR', '1D 1H, 0.5 mM, pH 7':'1H NMR', '1D 1H, 0.5 mM, pH 5':'1H NMR', '1D 1H, 100 mM, pH 2':'1H NMR', '1D 1H, 100 mM, pH 9':'1H NMR', '1D 1H, 100 mM, pH 5':'1H NMR', '1D 1H, 100 mM, pH 7':'1H NMR', '1D 1H, 0.5 mM, pH 9':'1H NMR', '1D 1H, 0.5 mM, pH 7.074':'1H NMR', '1D 1H, 2.0 mM':'1H NMR', '1D 1H, 0.5 mM':'1H NMR', '1D 1H, 0.5 mM, pH 6.836':'1H NMR', '1D DEPT135':'13C NMR', '1D 13C':'13C NMR', '1D DEPT90':'13C NMR', '2D J-resolved 1H':'2D NMR', '2D [1H,1H]-TOCSY':'2D NMR', '2D [1H,13C]-HSQC SW small':'2D NMR', '2D [1H,1H]-COSY':'2D NMR', '2D [1H,13C]-HMQC':'2D NMR', '2D [1H,13C]-HSQC':'2D NMR', '2D [1H,13C]-HMBC':'2D NMR', '2D [1H,13C]-HSQC SW expanded':'2D NMR', '1D Boric acid': '1D NMR other', '1D 1H, NOESY':'1H NMR'}



spectrum_types = set()

with open(outfile_name, "w") as outfile:
    for (rownum, row) in merged.iterrows():
        inchi = get_standard_inchi.standardize_inchi(row["InCHi_code"].strip())
        accession = row["Entry_ID"].strip()
        spectrum = row["Name_x"].strip()
        #spectrum_types.add(spectrum)
        spectrum = spectrum_map[spectrum]
        # print(inchi)
        # print(accession)
        # print(spectrum)
        #print(rownum)
        outfile.write(database_name + "\t" + inchi + "\t" + accession + "\t" + spectrum + "\n")
#for spec in spectrum_types:
#    print(spec)