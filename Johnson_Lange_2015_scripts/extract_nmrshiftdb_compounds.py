import get_standard_inchi
import re
import csv
import sys
csv.field_size_limit(sys.maxint)

record_dir = "E:/databases/for_review/nmrshiftdb"
outfile_name = "NMRShiftDB_compounds.tsv"
database_name = "NMRShiftDB"
#molecule_file_name = "molecules.txt"
spectra_file_name = "spectra.txt"

inchi_map_file_name = "nmrshiftdb_molecule_to_inchi.tsv"

spectrum_type_dict = {'1':'13C NMR', '2':'1H NMR', '3':'1D NMR other', '4':'1D NMR other', '5':'1D NMR other', '6':'1D NMR other', '7':'1D NMR other', '8':'1D NMR other', '9':'1D NMR other', '10':'1D NMR other', '11':'1D NMR other', '100':'1D NMR other', '101':'1D NMR other'}


id_to_inchi = dict() #keys are molecule_ID, values are InChI
with open(inchi_map_file_name, "r") as infile:
    for line in infile:
        line = line.strip()
        fields = line.split("\t")
        if len(fields) > 1:
            id_to_inchi[fields[0]] = fields[1]
        else:
            id_to_inchi[fields[0]] = ""
            print("No InChI for record %s " % fields[0])





with open(outfile_name, "w") as outfile:
    with open(record_dir + "/" + spectra_file_name, "r") as infile:
        for line in infile:
            match = re.search('^INSERT INTO `SPECTRUM` VALUES \((.+)\);$', line)
            if match:
                
                fields = list(csv.reader([match.group(1)], quotechar="'", escapechar="\\"))[0]
                spectrum_type = spectrum_type_dict[fields[7]]
                accession = fields[10]
                inchi = id_to_inchi[accession]
                if inchi != "":
                    outfile.write(database_name + "\t" + inchi + "\t" + accession + "\t" + spectrum_type + "\n")
            else:
                print("error reading line: %s" % line)