#import rdflib
#from rdflib import URIRef
import re
#from collections import OrderedDict
from pprint import pprint as pp
from copy import copy

compounds_filename = "ChEBI_compounds.tsv"
classes_filename = "ChEBI_classes.tsv"

def main():
  #g = rdflib.Graph()
  #result = g.parse("chebi.turtle")
  obo = Obo('chebi.obo')
  
  compounds = set()
  
  with open(compounds_filename, "w") as outfile:
    for rec in obo:
      #print(obo[rec]._data)
      inchi = obo[rec].inchi()
      if inchi:
        compounds.add(rec)
        outfile.write("ChEBI\t%s\t%s\tnone\n" % (inchi, rec))
  
  with open(classes_filename, "w") as outfile:
    for comp in compounds:
      ancestors = obo.ancestors(comp)
      outfile.write(comp + "\t")
      #if comp == 'CHEBI:42255':
      #  pp(ancestors)
      for anc in ancestors:
        outfile.write(anc + " " + obo[anc].name() + "\t")
      outfile.write("\n")
    
  
  
def test():
  obo = Obo('chebi.obo')
  print(obo['CHEBI:42255'].parents())
  print(obo.ancestors('CHEBI:42255'))

class Obo(object):
  def __init__(self, filename):
    '''
      Represents data internally as a dict of dicts with two entries: 'data', and 'type', 'data' is a dict of lists, type is a string.
      The record
      
      [Term]
      id: CHEBI:24867
      name: monoatomic ion
      synonym: "monoatomic ions" RELATED [ChEBI:]
      is_a: CHEBI:24870
      is_a: CHEBI:33238
      
      will parse to:
      {'CHEBI:24867':{'type': 'term', data:{'id':['CHEBI:24867'], 'name':['monoatomic ion'], 'synonym':['"monoatomic ions" RELATED [ChEBI:]'], 'is_a':['CHEBI:24870', 'CHEBI:33238']}}}
    '''
    self._records = dict()
    curr_id = ""
    curr_type = ""
    with open(filename, "r") as infile:
      for line in infile:
        line = line.strip()
        if line != "":
          if line[0] == '[' and line[-1] == ']':
            curr_id = ""
            curr_type = line[1:-1]
          elif curr_type != "":
            (field, part, data) = line.partition(': ')
            if curr_id == "" and field == "id":
              curr_id = data
              if curr_id not in self._records:
                self._records[curr_id] = Record(curr_id, curr_type)
            self._records[curr_id].add(field, data)
            
    
  def __getitem__(self, name):
    '''
      returns a list of values associated with the input field.
    '''
    out = None
    if name in self._records:
      out = self._records[name]
    return out
    
  def __contains__(self, key):
    return key in self._records
  
  def __iter__(self):
    return self._records.iterkeys()
    
  def ancestors(self, start):
    out = list()
    if start in self:
      seen = set()
      unseen = copy(self[start].parents())
      while len(unseen) > 0:
        root = unseen.pop()
        seen.add(root)
        for parent in self[root].parents():
          if not parent in seen:
            unseen.append(parent)
      out = list(seen)
    return out

class Record(object):
  def __init__(self, id="", type=""):
    self._data = dict()
    self.id = id
    self.type = ""
  
  def __getitem__(self, field):
    '''
      returns a list of values associated with the input field.
    '''
    out = list()
    if field in self._data:
      out = self._data[field]
    return out
    
  def __contains__(self, key):
    return key in self._data
    
  def add(self, field, datum):
    if field not in self._data:
      self._data[field] = list()
    self._data[field].append(datum)
  
  def first(self, field):
    if field in self._data:
      return self[field][0]
    else:
      return None
  
  def inchi(self):
    inchi = None
    for syn in self['synonym']:
      unquote = re.split(r'(?<!\\)"', syn)
      name = unquote[1]
      properties = unquote[2].split()
      if properties[1] == 'InChI':
        inchi = name
    return inchi
  
  def name(self):
    return self.first('name')
  
  def parents(self):
    return self['is_a']
  
  
if __name__ == "__main__":
  main()
  test()
