#
#   sum_equations.py, algorithms for finding all valid sums given a list of choices of numbers for each
#   position in the equation
#
######################################################################################################
#
# Sean Johnson, July 21, 2013
#


from copy import copy
from pprint import pprint as pp
import sqlite3
import cProfile

def enumerate_sums(l_lists, r_lists, l_sum=0, r_sum=0, solution_list=list()):
    '''
        runs a binary search through all combinations where one element is chosen from each of the input lists:
        finds all combinations such that (l_sum + sum(choices_from_l_lists)) = (r_sum + sum(choices_from_r_lists))
        
        l_lists: list of list of numbers. Each sublist represents the possible numbers for a given position
        on the left side of the equation.
        
        r_lists: list of lists of numbers. For the right side of the equation
        
        l_sum and r_sum: constants to be added to the left and right side (respectively) before comparison
        
        solution_list: a list of values that have been picked from members of l_lists and r_lists
            in the path leading up to this calling of enumerate_sums
        
        precondition: lists must be sorted! It also helps to remove duplicates, but that isn't absolutely essential
        (note that the algorithm can handle subtraction as well, just apply the minuses before sorting)
        
        Description:
            It's kind of a depth-first implicit enumeration algorithm and also resembles a binary search.
            
        Algorithm:
            starting with the first list in l_lists,
            pick the median and determine if there are any possible solutions
            starting with that number, or if all possible combinations starting
            with that number have the left_side larger than any possible right_side
            or vice versa.  If there are possible solutions, store those solutions,
            and ask the same question ("branch") about the numbers half-way higher and half-way
            lower in the list.  If it is a lower bound, only look higher, if it is an upper bound,
            only look lower.
            
            To know if a choice is an upper or lower bound, or not a bound, add the value of the choice
            to l_sum (or r_sum, if the current choice is from a right-hand list), and call enumerate_sums, 
            but with the list that the choice is from removed.  This chain will continue until there 
            are no more lists left in l_lists or r_lists (a depth-first traversal). Once there are no 
            possible choices, compare l_sum to r_sum and return a string indicating whether they are
            the same (and thus the path is a solution), or how they are different.
            
        
        TODO: The slowest parts of this algorithm are the "copy" statements.  Luckily, none of them are necessary.
            instead of copy and pop, at the beginning of the algorithm, a simple int could keep track of position in the lists.
            
            "copy" is also used to keep track of solutions, those "copy"s are also unnecessary.  Instead of building up the
            solution lists on the way "down" the recursion, the solution could just as easily be built on the way back "up", 
            which would be just as easy, and more efficient.
            
            I'll make those changes to this code pretty soon.
        
        Example:
            l = range(1,1000)
            l2 = range(1,100)
            (solution, tag) = enumerate_sums([l]*2, [l2]*1, 0, 0)
    '''
    queue = list()
    sols = list()
    
    top_list = None
    side = None
    if len(l_lists) > 0: 
        l_lists = copy(l_lists)
        top_list = l_lists.pop(0)
        side = "l"
    elif len(r_lists) > 0:
        r_lists = copy(r_lists)
        top_list = r_lists.pop(0)
        side = "r"
    else: #no more lists left, decide whether we are too high, too low, or just right
        if l_sum > r_sum:
            return (list(), "high") #l_sum is too high
        elif l_sum < r_sum:
            return (list(), "low") #r_sum is too low
        else:
            return ([solution_list], "no_bound")
    
    if len(top_list) == 0:
        return (list(), "error")
    
    queue.append(( 0, len(top_list)-1))
    low_present = False
    high_present = False
    no_bound_present = False
    while len(queue) > 0:
        (low, high) = queue.pop()
        mid = int((high+low)/2)
        value = top_list[mid]
        
        (new_sols, msg) = (None, None)
        if side == "l":
            new_solution_list = copy(solution_list)
            new_solution_list.append(value)
            (new_sols, msg) = enumerate_sums(l_lists, r_lists, l_sum+value, r_sum, new_solution_list)
        elif side == "r":
            new_solution_list = copy(solution_list)
            new_solution_list.append(value)
            (new_sols, msg) = enumerate_sums(l_lists, r_lists, l_sum, r_sum+value, new_solution_list)
        if msg == "error":
            return (set(), "error")
        elif msg == "high":
            high_present = True
            if side == 'l':
                if mid != low:
                    queue.append((low,mid-1))
            if side == 'r':
                if mid != high:
                    queue.append((mid+1,high))
            
        elif msg == "low":
            low_present = True
            if side == 'r':
                if mid != low:
                    queue.append((low,mid-1))
            if side == 'l':
                if mid != high:
                    queue.append((mid+1,high))

        elif msg == "no_bound":
            no_bound_present = True
            sols = sols + new_sols
            if mid != high:
                queue.append((mid+1,high))
            if mid != low:
                queue.append((low,mid-1))
    out_tag = "no_bound"
    if not no_bound_present:
        if (low_present and not high_present):
            out_tag = 'low'
        elif(high_present and not low_present):
            out_tag = 'high'
            
    return (sols, out_tag)

def enumerate_sums_sql(l_lists, r_lists, l_sum=0, r_sum=0):
    '''
        enumerates correct sums by createing a temporary sqlite database 
        then selecting values that make correct equations.
    '''
    
    #initialize database connection
    con = sqlite3.connect(':memory:')
    cur = con.cursor()
    
    l_tables = ['l'+str(x) for x in range(len(l_lists))]
    r_tables = ['r'+str(x) for x in range(len(r_lists))]

    #create the tables
    for table_name in l_tables + r_tables:
        command = "CREATE TABLE %s (value REAL)" % (table_name)
        cur.execute(command)
    
    for (num, data) in enumerate(l_lists):
        query = "INSERT INTO " + l_tables[num] + " (value) VALUES (?)"
        data = [(x,) for x in data]
        cur.executemany(query, data)
    for (num, data) in enumerate(r_lists):
        query = "insert into " + r_tables[num] + " (value) VALUES (?)"
        data = [(x,) for x in data]
        cur.executemany(query, data)
    
    
    #find the solutions
    query = "SELECT "
    query += ", ".join([table_name+".value" for table_name in l_tables + r_tables])
    query += " FROM "
    query += ", ".join([table_name for table_name in l_tables + r_tables])
    query += " WHERE "
    query += " + ".join([table_name + ".value" for table_name in l_tables])
    query += " + %d" % l_sum
    query += " = "
    query += " + ".join([table_name + ".value" for table_name in r_tables])
    query += " + %d" % r_sum
    
    #execute query and export solutions
    out = list()
    for row in cur.execute(query):
        out.append(row)
    return out

def main():
    #l = range(1,6)
    l = range(1,100)
    l2 = range(1,1000)
    (solution, tag) = enumerate_sums([l]*2, [l2]*1, 0, 0)
    pp(solution)
    #solution = enumerate_sums_sql([l]*2, [l2]*1, 0, 0)
    #pp(solution)


if __name__ == "__main__":
    #cProfile.run('main()')
    main()